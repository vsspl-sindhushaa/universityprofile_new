package UniversityProfile.UniversityProfile.Logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import UniversityProfile.UniversityProfile.to.UniversityProfileTo;

public class ParseHtml {
	public void parsePageSource(){
		try {
			Document doc = Jsoup.connect("http://www.4icu.org/reviews/11045.htm").get();
			UniversityProfileTo objProfile = new UniversityProfileTo();
			objProfile.setAbout(doc.select("div.col.span_1_of_2").get(0).getElementsByTag("p").text());
			// university details
			HashMap<String, String> universityDetails = new HashMap<String, String>();
			for (Element elem : doc.select("table[width=100%][cellpadding=3][align=right]").get(0).select("tr")) {
				if (null == universityDetails.get(elem.select("td").get(0).text()))
					universityDetails.put(elem.select("td").get(0).text(), elem.select("td").get(1).text());
			}
			objProfile.setUniversityName(universityDetails.get("University Name"));
			objProfile.setFoundedYear(universityDetails.get("Founded"));
			// location
			HashMap<String, String> locationDetails = new HashMap<String, String>();
			for (Element elem : doc.select("table[width=100%][cellpadding=3][align=right]").get(1).select("tr")) {
				if (elem.select("td").get(0).text().isEmpty())
					locationDetails.put("Contact Number", elem.select("td").get(1).text());
				else
					locationDetails.put(elem.select("td").get(0).text(), elem.select("td").get(1).text());
			}
			objProfile.setAddress(locationDetails.get("Address"));
			objProfile.setContactNumber(locationDetails.get("Contact Number"));
			// courses offered
			Elements rows = doc.select("table[cellpadding=0][cellspacing=1]").get(0).select("tr");
			ArrayList<String> objCourses = new ArrayList<String>();
			for (int i = 5; i < rows.size() - 1; i++) {
				String course = rows.get(i).select("td").get(0).text().replace((rows.get(i).select("td").get(0)
						.getElementsByTag("a").get(0).getElementsByTag("span").get(0).text()), "");
				objCourses.add(course);
			}
			objProfile.setCoursesOffered(objCourses);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String... args){
		new ParseHtml().parsePageSource();
	}
}
